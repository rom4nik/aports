# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=intel-media-driver
pkgver=22.4.2
pkgrel=0
pkgdesc="Intel Media Driver for VAAPI - Broadwell+ iGPUs"
options="!check" # tests can't run in check(), only on install
url="https://github.com/intel/media-driver"
arch="x86_64"
license="BSD-3-Clause AND MIT"
makedepends="
	cmake
	intel-gmmlib-dev
	libva-dev
	libpciaccess-dev
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/intel/media-driver/archive/intel-media-$pkgver.tar.gz"
builddir="$srcdir/media-driver-intel-media-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	# only recognises debug/release internally
	# BUILD_TYPE is an additional internal type
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Release \
		-DBUILD_TYPE=Release \
		-DINSTALL_DRIVER_SYSCONF=OFF \
		-DMEDIA_RUN_TEST_SUITE=OFF \
		$CMAKE_CROSSOPTS .
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1f32709b80cd25006e2156e4487d75d07ff5eb337164c6aaacb92385dc31c1ce0c80a3c7062c830f89674d70bfedd7f81e4f34ea4d1dd8adb4da0d2212e744ea  intel-media-driver-22.4.2.tar.gz
"
