# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=openmp
pkgver=14.0.4
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="LLVM OpenMP Runtime Library"
url="https://openmp.llvm.org/"
arch="all !s390x" # LIBOMP_ARCH = UnknownArchitecture
license="Apache-2.0"
options="!check" # 6 tests failing
depends_dev="$pkgname"
makedepends="
	clang
	cmake
	elfutils-dev
	libffi-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	perl
	samurai
	"
checkdepends="llvm$_llvmver-test-utils"
subpackages="$pkgname-dev"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/openmp-$pkgver.src.tar.xz"
builddir="$srcdir/$pkgname-$pkgver.src"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLIBOMP_INSTALL_ALIASES=OFF \
		-DCLANG_TOOL=/usr/bin/clang \
		-DLINK_TOOL=/usr/bin/llvm-link \
		-DOPT_TOOL=/usr/bin/opt \
		-DOPENMP_LLVM_TOOLS_DIR=/usr/lib/llvm$_llvmver/bin \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	cmake --build build --target check-openmp
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	rm -f "$pkgdir"/usr/lib/libarcher_static.a
}

sha512sums="
9979721f530d4ea0a10177dcfd781966dd83ac73da683ba89bbda4e922f75d1704c7bef6221c70ac5e3ee6a51ea3f0e7ab24fbf900769c483242e3c7e0181a13  openmp-14.0.4.src.tar.xz
"
