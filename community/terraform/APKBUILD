# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Sergii Sadovyi <serg.sadovoi@gmail.com>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=terraform
pkgver=1.2.2
pkgrel=0
pkgdesc="Building, changing and combining infrastructure safely and efficiently"
url="https://www.terraform.io/"
arch="all"
license="MPL-2.0"
makedepends="go"
checkdepends="openssh-client"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/terraform/archive/refs/tags/v$pkgver.tar.gz"
options="net"

export GOFLAGS="$GOFLAGS -modcacherw -mod=readonly -trimpath"
export GOPATH="$srcdir"

build() {
	go build -v -o bin/$pkgname \
		-ldflags "-X main.GitCommit=v$pkgver -X github.com/hashicorp/terraform/version.Prerelease="
}

check() {
	case "$CARCH" in
	arm*|x86)
		go list . | xargs -t -n4 \
			go test -timeout=2m -parallel=4
		;;
	*)
		go test ./...
		;;
	esac
	bin/$pkgname -v
}

package() {
	install -Dm755 "$builddir"/bin/$pkgname -t "$pkgdir"/usr/bin
}

sha512sums="
0bd56c2c281848a1f3e983e76a6ee13b7a483b02c35dcdf09084a2bee0aa39e9cc9380be35cb1b13cf895430c42117884c6bd2e27d487953d1fc73caac070f9f  terraform-1.2.2.tar.gz
"
