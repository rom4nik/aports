# Contributor: Grigory Kirillov <txgk@bk.ru>
# Maintainer: Grigory Kirillov <txgk@bk.ru>
pkgname=yyjson
pkgver=0.5.0
pkgrel=0
pkgdesc="High performance JSON library written in ANSI C"
url="https://github.com/ibireme/yyjson"
# lack of support for 32-bit arches
arch="all !x86 !armv7 !armhf !s390x"
license="MIT"
makedepends="cmake samurai"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="https://github.com/ibireme/yyjson/archive/$pkgver/yyjson-$pkgver.tar.gz"

build() {
	cmake -B builddir -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_SHARED_LIBS=ON \
		-DYYJSON_BUILD_TESTS=ON
	cmake -B builddir-static -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=MinSizeRel
	cmake --build builddir
	cmake --build builddir-static
}

check() {
	ctest --output-on-failure --test-dir builddir
}

package() {
	DESTDIR="$pkgdir" cmake --install builddir
	DESTDIR="$pkgdir" cmake --install builddir-static
	for file in README.md doc/API.md doc/BuildAndTest.md doc/DataStructure.md
	do
		install -Dm644 "$file" -t "$pkgdir"/usr/share/doc/"$pkgname"/
	done
}

sha512sums="
6b2e051f7d5829acb9ca25ce97e4872f5517b3f291a30c6a86ec40f71670b8114f43c2d272410834a5c92b5203726b65696f06423bc6e5001dff2aabf5eb20d9  yyjson-0.5.0.tar.gz
"
