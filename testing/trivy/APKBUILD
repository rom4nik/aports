# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.28.1
pkgrel=0
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
# ppc64le: FTBFS: build constraints exclude all Go files in [...]
# riscv64: modernc.org/libc@v1.14.1 build constraints exclude all Go files
arch="$arch !s390x !ppc64le !riscv64"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"

build() {
	make build VERSION=$pkgver
}

check() {
	make test
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
03a01e66edcb09b4c47da086b433ad4fd762dfc6211132552ec9a4607b99215eb5dc8669bf8e01b917533305e85d6e795171c7c30e8c05c0dc052150effa1f28  trivy-0.28.1.tar.gz
"
